import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainerQuestionPlusPage } from './trainer-question-plus';

@NgModule({
  declarations: [
    TrainerQuestionPlusPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainerQuestionPlusPage),
  ],
})
export class TrainerQuestionPlusPageModule {}
