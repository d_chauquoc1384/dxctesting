import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
//import storage
import { Storage} from '@ionic/storage';
import { ViewChild } from '@angular/core';
import firebase from 'firebase';
import { TranerUpdateExamPage } from '../traner-update-exam/traner-update-exam';

/**
 * Generated class for the TrainerQuestionPlusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trainer-question-plus',
  templateUrl: 'trainer-question-plus.html',
})
export class TrainerQuestionPlusPage {
  @ViewChild('myFile') 
  myFileVar: any;

  examData: any = {};
  examKey: string = "";
  hasImg: boolean = false;
  image : any = "ifany";
  constructor(private alertCtrl: AlertController, public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, public af: AngularFireDatabase) {

    if (this.navParams.get('examKey')) {
      this.examKey = this.navParams.get('examKey');
      this.examData = this.navParams.get('data');
    }
  }
  qPlusForm(form: NgForm) {
    if (form.valid) {
        let dataThis = {};
        let dataUpdate = [];
        let text = (form.controls['questionText']) ? form.controls['questionText'].value : "";
        let limitTime = (form.controls['answerSetTime']) ? form.controls['answerSetTime'].value : "";
        let type = (form.controls['answerType']) ? form.controls['answerType'].value : "";

        let answer = [];
        for (var i = 0; i < 4; i ++) {

          answer.push({
            correct: (form.controls['answerValid_'+i]) ? form.controls['answerValid_'+i].value : "",
            selected: 'false',
            text: (form.controls['answerText_'+i]) ? form.controls['answerText_'+i].value : ""
          })
        }
        dataThis = {
          text: text,
          limitTime: limitTime,
          type: type,
          image: this.image,
          answer: answer
        }
        if (this.examData.question.length) {
          this.examData.question.forEach(element => {
            dataUpdate.push(element);
          });
        }
        dataUpdate.push(dataThis);
        console.log('dataThis');
        console.log(dataThis);
        console.log('dataUpdate');
        console.log(dataUpdate);
        let loader = this.loadingCtrl.create({
          content: "Creating a question...",
          duration: 500
        });
        loader.present();
        this.af.database.ref('exams/'+this.examKey+'/question').update(dataUpdate).then((val)=> {
          loader.data.content = 'Create success!';
          this.navCtrl.push(TranerUpdateExamPage, {
            examkey: this.examKey
          });
        });
       // console.log(dataThis);
    }
  }

  uploadImage() {
    if (this.hasImg) { 
    let storageRef = firebase.storage().ref();
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
    for (let selectedFile of [(<HTMLInputElement>document.getElementById('questionImg')).files[0]]) {
      let path = `/uploads/${selectedFile.name}`;
       var iRef = storageRef.child(path).put(selectedFile);
       if (iRef) {
          iRef.then((snapshot)=> {
            this.image = snapshot.metadata.downloadURLs[0];
            loader.data.content = "Success!";
            setTimeout(() => {
              loader.dismiss();
            }, 500);
        });
       }else {
          loader.data.content = "Upload fai, please upload again!";
          setTimeout(() => {
            loader.dismiss();
          }, 500);
       }
      
    }
    this.hasImg = false;
  }

  
  }
  removeImg () {
    if ((<HTMLInputElement>document.getElementById('questionImg')).files[0] != undefined) {
     this.myFileVar.nativeElement.value = "";
     this.hasImg = false;
    } 
  }
  someChange() {
    if ((<HTMLInputElement>document.getElementById('questionImg')).files[0] != undefined) {
      this.hasImg = true;
    } else this.hasImg = false;
  }
  pop() {
    this.navCtrl.pop();
  }
}
