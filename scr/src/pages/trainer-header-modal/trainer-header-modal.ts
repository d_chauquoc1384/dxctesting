import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
//import storage
import { Storage} from '@ionic/storage';

import { 
  FormGroup, 
  FormBuilder, 
  Validators, 
  NgForm
} from '@angular/forms';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { TranerUpdateExamPage } from '../traner-update-exam/traner-update-exam';

/**
 * Generated class for the TrainerHeaderModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trainer-header-modal',
  templateUrl: 'trainer-header-modal.html',
})
export class TrainerHeaderModalPage {

  data: any = {};
  examKey: any = {};
  settimeVal: any = "";
  userName: any = [];
  userList: any = [];
  userListKey: any = [];
  constructor(private loadingCtrl: LoadingController, public af: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams) {

    if (this.navParams.get('data') && this.navParams.get('examKey')) {
      this.data = this.navParams.get('data');
      this.examKey = this.navParams.get('examKey');


      if (this.data.totalTime != '') {
        this.settimeVal = 'timeTotal';
      } else this.settimeVal = 'timeEach';

      this.userListKey = [];
      if (this.data.userInvited != '' && this.data.userInvited != undefined) {
        this.data.userInvited.forEach(element => {
          this.userListKey.push(element);
          this.af.database.ref('users').orderByKey().equalTo(element).on('value', snapshot=> {
           
            snapshot.forEach(snapItemName=>{
              this.userName.push(
                {
                  userid: element,
                  fullname: snapItemName.val().fullname
                }
              )
              return false;
            })
          })
        });
      }

    
    } // If Exist data, key
  }

  headerForm(form: NgForm) {
    if (form.valid) {
      let data = {};

      if (form.controls['settime'].value == 'totalTime') {
        this.data.question.forEach(element=> {
          element.limitTime = "";
        })
      }
      data = {
        title: form.controls['title'].value,
        passScore: form.controls['passScore'].value,
        totalTime: (form.controls['totalTime']) ? form.controls['totalTime'].value : "",
        counterType: form.controls['settime'].value,
        date: this.data.date,
        user: this.data.user,
        scores: this.data.scores,
        userInvited: this.userListKey,
        question: (this.data.question)? this.data.question : ""
      }
    

      let loader1 = this.loadingCtrl.create({
        content: "Updating...",
        duration: 3000
      });
      loader1.present();
      this.af.database.ref('exams/'+this.examKey).update(data).then((val)=> {
        loader1.data.content = "Success!";
        setTimeout(() => {
          loader1.dismiss();
        }, 500);
        this.navCtrl.push(TranerUpdateExamPage, {
          examkey: this.examKey
        });
      });

    }

  }
  
  removeUserInvited(userid: string, i: number) {
    this.userName.splice(i,1);
    this.userListKey = [];
    this.userName.forEach(element => {
      this.userListKey.push(element.userid);
    });
  }
  pop() {
    this.navCtrl.pop();
  }
  showUsers() {
    this.af.database.ref('users').on('value', snapshot=> {
      let users = [];
      snapshot.forEach(snapItem=> {
        users.push({
          userid: snapItem.key,
          fullname: snapItem.val().fullname
        })
        return false;
      })
      this.userList = users;
    })
  }

  initializeItems(): void {
    this.showUsers();
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.userList = this.userList.filter((item) => {
        if (item.fullname) {
          return (item.fullname.toLowerCase().indexOf(val.toLowerCase()) > -1);
        }
      })
    }
  }
  getUserInvited(userid: string, fullname: string) {
    if (!userid || !fullname) {
      return false;
    }
    this.userListKey = [];
    this.userName.forEach(element => {
      this.userListKey.push(element.userid);
    });

    if (this.userListKey.indexOf(userid) > -1) {
      return false;
    }
    this.userListKey.push(userid);
    
    this.userName.push({
      userid: userid,
      fullname: fullname
    })
  }
}
