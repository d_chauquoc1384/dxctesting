import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainerHeaderModalPage } from './trainer-header-modal';

@NgModule({
  declarations: [
    TrainerHeaderModalPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainerHeaderModalPage),
  ],
})
export class TrainerHeaderModalPageModule {}
