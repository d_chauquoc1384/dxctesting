import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ExamPage } from '../exam/exam';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Storage} from '@ionic/storage';
import { ResultListPage } from '../result-list/result-list';
/**
 * Generated class for the TraineeListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trainee-list',
  templateUrl: 'trainee-list.html',
})
export class TraineeListPage {
  itemsRef: AngularFireList<any>;
  items: Observable<any[]>;
  public uc_items=[];
  public c_items=[];
  public uc_items_key=[];
  public c_items_key=[];
  userid;
  userinvited=false;
  fullname: any = "";
  public tog=true;

//----------------

  invited: any = [];
  invitedKey: any = [];
  todo: any = [];
  done: any = [];


  constructor(public navCtrl: NavController, public navParams: NavParams,public af: AngularFireDatabase,public storage:Storage) {
  
    this.storage.get('userid').then((val)=>{
      this.af.database.ref('exams').on('value',snapshot=>{  
        //console.log(snapshot.val());
        snapshot.forEach(snapItem => {
          if(snapItem.val().userInvited){
            //console.log(snapItem.val());

            snapItem.val().userInvited.forEach(element => {
              if (element == val) {
                this.invited.push({
                  value: snapItem.val(),
                  examKey: snapItem.key
                });
              }
            });
            }
          return false;
        });

        //console.log(this.invited);
     })


      this.invited.forEach(element => {
          //console.log(element.value);
         this.invitedKey.push(element.examKey);

          if (element.value.scores != "") {
            element.value.scores.forEach(elementScore => {
              if (elementScore != "") {
  
                if (elementScore.id == val) {
                  // Exist in Scores
                  this.done.push(element.examKey)
                }
              } else console.log('null');
            });
          }
      });

      for (var i = 0; i < this.invitedKey.length; i ++) {

        console.log(this.done);
        
        if (this.done.indexOf(this.invitedKey[i]) == -1) {
          this.todo.push(this.invitedKey[i]);
        }
      }

      console.log('Duoc moi');
      console.log(this.invitedKey);
      console.log(this.done);


      console.log('chua lam');
      console.log(this.todo);
      

      // Lam roi

      for (var i = 0; i < this.done.length; i ++) {
        this.af.database.ref('exams').orderByKey().equalTo(this.done[i]).on('value', snapshot=> {
          var index = i;
          snapshot.forEach(snapItem=> {
            this.c_items.push({
              key: this.done[index],
              value: snapItem.val()
            })
            return false;
          })

        })
      } 
      for (var i = 0; i < this.todo.length; i ++) {
        this.af.database.ref('exams').orderByKey().equalTo(this.todo[i]).on('value', snapshot=> {
          var index = i;
          snapshot.forEach(snapItem=> {
            
            this.uc_items.push({
              key: this.todo[index],
              value: snapItem.val()
            })
            return false;
          })

        })
      } 
      this.c_items.reverse();
      this.uc_items.reverse();
      console.log(this.c_items);
      console.log(this.uc_items);
    });
  }

  ionViewDidLoad() {
   
     // snapItem.val().userInvited.forEach(element1=>{
            //   if(element1==val&&snapItem.val().scores){
            //     //this.userinvited=true;
            //     snapItem.val().scores.forEach(element => {
            //       //console.log(element.id);
            //       if(element.id==val){
            //             ////console.log(i);
            //             this.c_items.push({
            //               key: snapItem.key,
            //               value: snapItem.val()
            //             });
                      
            //           }else{
            //             this.uc_items.push({
            //               key:snapItem.key,
            //               value: snapItem.val()
            //             });
                        
            //           }
            //       });
            //     } 
            //   });
    
  
 //console.log(this.c_items);
  }

  getName(userid: string) {
    if (!userid) {
      return false;
    }
    
    this.af.database.ref('users/'+userid+'/fullname').on('value', data=> {
      ////console.log(data.val());
      this.fullname = data.val();
    })
     return true;
  }
  toggleT(){
   
    this.tog=true;
  }
  toggleF(){
    this.tog=false;
  }
  exam_c(c_key,c_value){
    this.navCtrl.push(ExamPage, {
      key: c_key,
      value: c_value
    });
    //console.log(c_key);
  }
  exam_uc(uc_key,uc_value){
    this.navCtrl.push(ExamPage, {
      key: uc_key,
      value: uc_value
    });
  }
  view(key,value){
    this.navCtrl.push(ResultListPage,{
      key:key,
      value:value
    })
  }

}
