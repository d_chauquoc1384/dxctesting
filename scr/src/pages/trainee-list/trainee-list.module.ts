import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TraineeListPage } from './trainee-list';

@NgModule({
  declarations: [
    TraineeListPage,
  ],
  imports: [
    IonicPageModule.forChild(TraineeListPage),
  ],
})
export class TraineeListPageModule {}
