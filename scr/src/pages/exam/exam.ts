import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides} from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AngularFireDatabase } from 'angularfire2/database';
import { TraineeListPage } from '../trainee-list/trainee-list';
import { Storage} from '@ionic/storage';


export interface displaytimer {
  seconds: number;
  secondsRemaining: number;
  runTimer: boolean;
  hasStarted: boolean;
  hasFinished: boolean;
  displayTime: string;
 
}


/**
 * Generated class for the ExamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-exam',
  templateUrl: 'exam.html',
})
export class ExamPage {
  
  timeInSeconds: number;
  displaytimer: displaytimer;
  @ViewChild('slides') slides: Slides;
 
hasAnswered: boolean = false;
score: number = 0;
per:number=0;
testtime:number;
translate;
questions: any;
timer: any;
toTick:any;
data:any = {
  key: "",
  value: ""
}

public a:any;
public b:any;
public count:number=0;
public itemsRef ;
public scoreRef;
public index;
public userid: string = "";
public totallimitTime:number=0;
public sc=[];
public getsc:any=[];
public pass;
public update=false;
private storage: Storage;

  constructor(public navCtrl: NavController, public navParams: NavParams,public af:AngularFireDatabase,storage:Storage) {
    this.storage=storage;
    this.data.key=this.navParams.get('key');
    this.data.value=this.navParams.get('value');
    //////console.log(this.data.value.question.length);
    this.index=0;
    let user="";
    //console.log(this.data.key);
    this.storage.get('userid').then((val)=>{
       user=val;
       this.userid = val;
      //console.log(user);
      this.scoreRef.on('value',snapshot=>{
        snapshot.forEach(snapItem=>{
          this.getsc.push(snapItem.val());
          //console.log(snapItem.val());
          //console.log(user);
          if(snapItem.val().id==user){
            this.sc=snapItem.key;
            //console.log(this.sc);
            this.update=true;
          }else{this.update=false;}
        });
      });

      
    });
    //console.log(this.userid);
    if(this.data.value.totalTime!=""){
      this.timeInSeconds=this.data.value.totalTime;
    }else{
      this.timeInSeconds=this.data.value.question[this.index].limitTime;
      //console.log(this.index);
      for(var i=0;i<this.data.value.question.length;i++){
        this.totallimitTime+=Number(this.data.value.question[i].limitTime);
        ////console.log(this.totallimitTime);
       ////console.log(this.data.value.question[i].limitTime);
      }
    }
    this.itemsRef = this.af.database.ref('exams/'+this.data.key).child('question/');

    this.scoreRef = this.af.database.ref('exams/'+this.data.key).child('scores/');
  
    
   
    this.a=[];
    this.b=[];
    
  }

 ionViewDidLoad() {
  
  }
  slideChanged(){
  
     this.stoptimerTick();
     // Change total time to time per question
    if(this.data.value.totalTime==""&&!this.slides.isBeginning()){ 
      this.initTimer();
      this.resettimer();
    }
    
    if(!this.slides.isEnd()){
      ////console.log(this.data.value.question[this.index].limitTime);
      this.index++;
      this.startTimer();
    }else{
      this.per=Math.round(this.score*100/this.data.value.question.length);
      if(this.data.value.totalTime!=""){
        this.testtime=this.timeInSeconds-this.displaytimer.secondsRemaining;
      }else{this.testtime=this.totallimitTime-this.displaytimer.secondsRemaining;}
      //console.log(this.data.key);
      this.af.database.ref('exams/'+this.data.key).on('value',snapshot=>{
        //console.log(snapshot.val());
        if(this.per>=snapshot.val().passScore){
          return this.pass=true;
        }else{
          return this.pass=false;
        }
        //////console.log(snapshot.val());
    });
    }
    
    //if(!this.slides.isBeginning()){this.resultMultiAnswer(this.data.question);}
    //this.resultMultiAnswer(this.data.question);
  }

  //Under Timing------------------------------
  nextSlide(){
    this.slides.slideNext();
  }
  start(){
    setTimeout(()=>{
      this.slides.slideNext();
    },0);
    this.starttimer();
    this.initTimer();
    
  }
  stoptimer(){
    ////console.log("Stop timer");
    clearInterval(this.timer);
  }
  starttimer(){
    ////console.log("Start timer");
      this.timer=setInterval(()=>{
        this.slides.slideNext();
        //if(!this.slides.isBeginning()){this.resultMultiAnswer(this.data.question.text);}
      },this.timeInSeconds*1000+1000);
    }

  resettimer(){
    this.stoptimer();
    this.starttimer();
  }
  // selectAnswer(answer,question){
  //   this.hasAnswered = true;
  //   answer.selected=true;
  //   if(answer.correct){
  //     this.score++;
  //   }
  //   setTimeout(()=>{
  //     this.hasAnswered=false;
  //     this.slides.slideNext();
  //     answer.selected=false
  //   },500);
  // }
  selectAnswer(answer,question){
    this.hasAnswered = true;
    answer.selected=true;
    if(answer.correct=='true'){
          this.score++;
          ////console.log(this.score);
         // this.per=this.score*100/this.data.value.question.length;
         }
    setTimeout(()=>{
      this.hasAnswered=false;
      this.slides.slideNext();
      answer.selected=false;
    },500);
    //if(this.data.totalTime==0){this.resettimer();}
    
  }
  selectMultiAnswer(answer){
    if(!answer.selected){
      answer.selected=true;
      this.a.push(answer);
      ////console.log(this.a);
    }else{
      answer.selected=false;
       for(var i=0;i<this.a.length;i++){
         if(!this.a[i].selected){this.a.pop(this.a[i]);}
       }
    }
    return this.a;   
  }
  resultMultiAnswer(question){
    ////console.log(question);
    let m_b=this.b;
    let m_a=this.a;
    let m_tf=true;
   this.itemsRef.orderByChild('text').equalTo(question).on('child_added',function(data){
     var c=data.val().answer;
     ////console.log(c);
     for(var i=0;i<c.length;i++){
       if(c[i].correct=="true"){
         m_b.push(c[i]);
         ////console.log(i+m_b);
       }
     }
     ////console.log(m_b);
     ////console.log(m_a);
   });
   if(m_a.length==m_b.length){
    for(var i=0;i<m_a.length;i++){
      if(m_a[i].correct=="false"){m_tf= false;}
    }
    if(m_tf){this.score++;  };
    
   }
     setTimeout(()=>{
      for(var i=0;i<m_a.length;i++){
        
        m_a[i].hasAnswered=false;
        m_a[i].selected=false;
      }
      this.slides.slideNext();
    },500);
    //if(this.data.totalTime==0){this.resettimer();}
   
     // for(var i=0;i<m_b.length;i++){m_b.pop(m_b[i]);}

   }
    
  // pass(){
  //   this.per=this.score*100/this.data.value.question.length;
  
  // }
  
resetscore(){
  this.score=0;
}
  restartQuiz(){
    
    this.score;
    this.per;
   // ////console.log(this.data.value.question.length);
   
   if(this.update){
    
    this.af.database.ref('exams/'+this.data.key).child('scores/'+this.sc).update({
      id : this.userid,
      score:this.score,
      per:this.per,
      pass:this.pass
    });
   }
   else{
     //console.log(this.update);
     //console.log(this.userid);
     //console.log(this.score);
     //console.log(this.per);
     //console.log(this.pass);
    this.getsc.push({
      id : this.userid,
      score:this.score,
      per:this.per,
      pass:this.pass
    });
    this.scoreRef.set(this.getsc);
   }
      
    
  
    
    this.stoptimer();
    this.stoptimerTick();
    this.navCtrl.setRoot(TraineeListPage);
    this.resetscore();
  }
  //Display Total Timing--------------------------------
  initTimer() {
    if (!this.timeInSeconds) { 
      this.timeInSeconds = 0; 
      
    }else{
      ////console.log("time insecond"+this.timeInSeconds);
    }

    this.displaytimer = <displaytimer>{
      seconds: this.timeInSeconds,
      runTimer: false,
      hasStarted: false,
      hasFinished: false,
      secondsRemaining: this.timeInSeconds
    };

    this.displaytimer.displayTime = this.getSecondsAsDigitalClock(this.displaytimer.secondsRemaining);
    console.log("second remaining"+this.displaytimer.secondsRemaining);
  }
  pauseTimer() {
    this.timer.runTimer = false;
  }
  startTimer() {
    
    this.displaytimer.hasStarted = true;
    this.displaytimer.runTimer = true;
    this.timerTick();
  }
  stoptimerTick(){
    clearTimeout(this.toTick);
  }
  timerTick() {
   this.toTick= setTimeout(() => {
      if (!this.displaytimer.runTimer) { 
        ////console.log("Ko chay stop stop!!!!!!");
        return;
       }
      this.displaytimer.secondsRemaining--;
      this.displaytimer.displayTime = this.getSecondsAsDigitalClock(this.displaytimer.secondsRemaining);
      ////console.log("remaining"+this.displaytimer.secondsRemaining)
      if (this.displaytimer.secondsRemaining > 0) {
        this.timerTick();
        //////console.log(this.slides.length());
      } else{
        this.displaytimer.hasFinished = true;
        if(this.data.value.totalTime!=0){
          this.slides.slideTo(this.slides.length(),0);
        }
      }
    }, 1000);
  }
  getSecondsAsDigitalClock(inputSeconds: number) {
    const secNum = parseInt(inputSeconds.toString(), 10); // don't forget the second param
    const hours = Math.floor(secNum / 3600);
    const minutes = Math.floor((secNum - (hours * 3600)) / 60);
    const seconds = secNum - (hours * 3600) - (minutes * 60);
    let hoursString = '';
    let minutesString = '';
    let secondsString = '';
    hoursString = (hours < 10) ? '0' + hours : hours.toString();
    minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
    secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
    return hoursString + ':' + minutesString + ':' + secondsString;
  }
}
