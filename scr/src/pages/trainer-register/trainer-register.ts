import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { NgIf } from '@angular/common';
import { 
  FormGroup, 
  FormBuilder, 
  Validators 
} from '@angular/forms';

import { AngularFireDatabase } from 'angularfire2/database';
import { TrainerListPage } from '../trainer-list/trainer-list';
import { TrainerCreateExamPage } from '../trainer-create-exam/trainer-create-exam';
import { Storage} from '@ionic/storage';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';

/**
 * Generated class for the TrainerRegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trainer-register',
  templateUrl: 'trainer-register.html',
})
export class TrainerRegisterPage {

  public form: FormGroup;
  public items: Array<any> = [];
  public itemsRef = this.af.database.ref('users');
  public errForm: any;
  public show = false;
  public emaiExist: Array<any> = [];
  public emaiExistErr: boolean = true;
  constructor(public store : Storage, public loadingCtrl: LoadingController, public af: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      fullname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern('(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%])[A-Za-z0-9@#$%]{8,}')]],
      re_password: ['', [Validators.required,Validators.pattern('(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%])[A-Za-z0-9@#$%]{8,}')]]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrainerRegisterPage');
  
  }
  isValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }



  presentLoadingDefault() {
    
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
  
    setTimeout(() => {
      loading.dismiss();
    }, 500);
  }
  trainerForm() {
    if (this.form.valid === true) {
     
      if (this.form.value.password === this.form.value.re_password) {
        //Load user email
        let exist = false;
        this.af.database.ref('users').on('value', snapShot=> {
          let users = [];
          snapShot.forEach(snapItem => {
            users.push(snapItem.val().email);
            return false;
          });
          if (users.indexOf(this.form.controls['email'].value) > -1) {
            exist = true;
          }
        });

        if (exist == false) {

          this.show = false;
          var data: any;
          data = {
            fullname: this.form.value.fullname,
            email: this.form.value.email,
            password: this.form.value.password,
            trainer: 1
          };
          let loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 500
          });
          loader.present();
          this.itemsRef.push(data).then((val)=> {
            loader.data.content = "Register success!";
            this.store.set('userid',val.key).then((val)=> {
              this.navCtrl.push(LoginPage);
            })
            
          });
          
        } else {
          let loader = this.loadingCtrl.create({
            content: "Email already this databse",
            duration: 500
          });
          loader.present();
        }
   
         

     } else { // Password confirm
       this.show = true;
       console.log(this.show);
     }
   } //form valid
  }
  trainerCreateExam() {
    this.navCtrl.push(TrainerCreateExamPage);
  }
  addData() {
    var dataExam = {
      userid: "-L6oO0RsWkrlFJh70oCp",
      date: "6/3/2018"
    }

    var examRef = this.af.database.ref('exams');
    examRef.push(dataExam);

  }
  addQuestion() {
    var dataQuestion = {
      content: "This is question text?",
      examKey: "-L6uIYXrcocDxkhZwwcT",
      selectType: "checkbox",
      image: ""
    }
    var questionRef = this.af.database.ref('questions');
    questionRef.push(dataQuestion);
  }
  addAnswer() {
    var dataAns = {
      content: "Answer text ...",
      questionKey: "-L6uUfJaZyxYlPRHOnBo",
      correct: 1
    }
    var ansRef = this.af.database.ref('answers');
    ansRef.push(dataAns);
  }

  trainerExamList() {
    this.navCtrl.push(TrainerListPage);
  }
  
}
