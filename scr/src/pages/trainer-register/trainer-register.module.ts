import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainerRegisterPage } from './trainer-register';

@NgModule({
  declarations: [
    TrainerRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainerRegisterPage),
  ],
})
export class TrainerRegisterPageModule {}
