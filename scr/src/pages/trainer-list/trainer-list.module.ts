import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainerListPage } from './trainer-list';

@NgModule({
  declarations: [
    TrainerListPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainerListPage),
  ],
})
export class TrainerListPageModule {}
