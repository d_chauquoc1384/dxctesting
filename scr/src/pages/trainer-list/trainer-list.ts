import { Component, OnInit, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ItemSliding  } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Reference } from '@firebase/database';
//import { ExamDetailPage } from '../exam-detail/exam-detail';
//import * as $ from "jquery";
import { TrainerCreateExamPage } from '../trainer-create-exam/trainer-create-exam';
import { NgModel } from '@angular/forms';
import { ExamPage } from '../exam/exam';
import { TranerUpdateExamPage } from '../traner-update-exam/traner-update-exam';
/**
 * Generated class for the TrainerListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

import { Storage} from '@ionic/storage';
import { ViewResultPage } from '../view-result/view-result';

@IonicPage()
@Component({
  selector: 'page-trainer-list',
  templateUrl: 'trainer-list.html',
})

export class TrainerListPage {

  p: number = 1;
  
  itemsRef: AngularFireList<any>;
  items: Observable<any[]>;
  totalItems: number = 0;
  itemStore: any = [];
  itemScroll: any = [];
  itemPerPage: number = 11;
  currentPage: number = 1;
  checkboxShow: boolean = false;
  userid: any = "";
  userJoin: number = 0;
  

  constructor(private storage: Storage, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public af: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams) {

      this.userid = this.navParams.get('userid');
        if (this.userid != undefined) {
        this.af.database.ref('exams').orderByChild('user').equalTo(this.userid).on('value', snapshot=> {
          let counter = 0;
          let scroll = [];
          let scores = 0;
          snapshot.forEach(snapItem=> {
            counter += 1;
            let userDone = [];
            this.userJoin = 0;
            if (snapItem.val().scores) {
                snapItem.val().scores.forEach(element => {
                if (element != '') {
                  userDone.push(element.id);
                }
              });
            }
            scroll.push({
              key: snapItem.key,
              value: snapItem.val(),
              score: userDone.length
            });
            return false;
          });
          this.itemStore = scroll.reverse();

          
          this.totalItems = counter;
          if (scroll.length > this.itemPerPage) {
            for (var i = 0; i < 11; i ++) {
              this.itemScroll.push(this.itemStore[i]);
            }
          } else {
            this.itemScroll = this.itemStore;
          }

        })
      }
    // this.items = this.af.list('exams', ref=>
    // ref.orderByChild('userid').equalTo('-L6oO0RsWkrlFJh70oCp')
    // ).snapshotChanges().map(changes=> {
    //   return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    // });
  }


  //---Exam edit

  edit(key: string) {
    
    if (!key) {
      return false;
    }
    this.navCtrl.push(TranerUpdateExamPage, {
      examkey: key
    });

  }

  //---Update Exam
  updateExamPage(key: string) {
    if (!key) {
      return false;
    }
    this.navCtrl.push(TranerUpdateExamPage, {
      key: key
    });
  }

  viewResult(examkey: string) {
    if (!examkey) {
      return false;
    }

    this.navCtrl.push(ViewResultPage, {
      key: examkey
    });

  }

 
  removeAll() {
    if(!this.checkboxShow) {
      this.checkboxShow = true;
    } else {
      this.checkboxShow = false;
    }
  }
  doInfinite(infiniteScroll) {
      this.currentPage += 1;

      let startAt = this.itemPerPage*(this.currentPage -1);
      let maxAt = startAt + this.itemPerPage;
      setTimeout(() => {
        for (var i = startAt; i < maxAt; i ++ ) {
          if (i < this.itemStore.length) {
            this.itemScroll.push(this.itemStore[i]);
          }
        }
        infiniteScroll.complete();
      }, 500);

      console.log('Current page: '+this.currentPage);
      console.log('StartAt: '+startAt);
      console.log('MaxAt: '+maxAt);
      console.log(this.itemScroll.length);

  }

  share(itemSlide: ItemSliding) {
    itemSlide.getOpenAmount();
  }

  deleteOnly(key: string, i: number) {
    if (!key) {
      return false;
    }
    // let loading = this.loadingCtrl.create({
    //   content: 'Deleting ...'
    // });
    // loading.present();
    // this.af.database.ref('exams/'+key).remove().then((result)=> {
    //   setTimeout(() => {
    //   loading.dismiss()
    //   }, 500);
    // })
    
    let confirm = this.alertCtrl.create({
      title: 'Are you sure to delete this exam?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
          }
        },
        {
          text: 'Agree',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Deleting ...'
            });
            loading.present();
            this.af.database.ref('exams/'+key).remove().then((result)=> {
              setTimeout(() => {
              this.itemScroll.splice(i,1);
              loading.dismiss()
              }, 500);
            })
          }
        }
      ]
    });
    confirm.present();
  

  }
  // alertKey() {
  //   $(document).ready(function() {
  //     alert($('.btnId').attr('id'));
  //   })
  // }

  ExamCreate() {
    this.navCtrl.push(TrainerCreateExamPage);
  }

  detailExamDirect(examKey: string) {
    this.navCtrl.push(ExamPage, {
      key: examKey
    })
  } 
}
