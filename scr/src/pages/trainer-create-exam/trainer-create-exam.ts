import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
//import * as $ from "jquery";
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Reference } from '@firebase/database';
//import { ExamDetailPage } from '../exam-detail/exam-detail';
import { 
  FormGroup, 
  FormBuilder, 
  Validators, 
  NgForm
} from '@angular/forms';
//import { Camera, CameraOptions } from '@ionic-native/camera';
import firebase from 'firebase';
import { TrainerListPage } from '../trainer-list/trainer-list';
//import storage
import { Storage} from '@ionic/storage';

/**
 * Generated class for the TrainerCreateExamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trainer-create-exam',
  templateUrl: 'trainer-create-exam.html',
})
export class TrainerCreateExamPage {
 
  form: NgForm;
  public itemsRef = this.af.database.ref('users');
  templates: string = "";
  toggleNew: boolean = false;
  counter: any;
  counterAnswer: any;
  GroupValid: any;
  settime: any = {};
  questionText: any = {};
  userInvited: any = [];
  uploadSwitch: boolean = false;
  submitSwitch: boolean = false;
  userid: any = "";
  usertag: any = [];

  public imageGroupUpload:Array<any> = [];

  items: Observable<any[]>;

  public userList:Array<any> = [];
  public loadedUserList:Array<any> = [];
  public userRef:firebase.database.Reference;

  captureDataUrl: string;

  constructor(public loadingCtrl: LoadingController, public af: AngularFireDatabase , public _fb: FormBuilder,public navCtrl: NavController, public navParams: NavParams,private storage: Storage) {
    this.counter = [
      0
    ];
    
    this.counterAnswer = [];
    this.counterAnswer[0] = [1,2,3,4];
    this.storage.get('userid').then((val)=>{
      this.userid = val;
      this.imageGroupUpload[0] = 'ifany';
    });

    
  }

  //-- Upload image btn click
  uploadWhenClick() {
    this.uploadSwitch = false;
  }

  //--File change
  someChange(i: number) {
    
    // For loop file input, check its exist file
    for (var i = 0; i < this.counter.length; i ++) {
      this.uploadSwitch = false;
      if ((<HTMLInputElement>document.getElementById('questionImg_'+i)).files[0] != undefined) {
        this.uploadSwitch = true;
        break;
      }
    }
  }

  removeTag(i: number) {
    if (!i) {
      return false;
    }
    this.userInvited.splice(i,1);
    this.usertag.splice(i,1);
  }

  //---Insert userid to array
  getKeyUSerInvited(key: string, fullname: string) {

    if (this.userInvited.indexOf(key) > -1) {
      return false;
    }
    if (key && fullname) {
      this.userInvited.push(key);
      this.usertag.push(fullname);
    }
    
  }


 
  //---Search invite users!
  initializeItems(): void {
    this.userRef = firebase.database().ref('users');
    this.userRef.on('value', snapshot=> {
      let users = [];
      
      snapshot.forEach(snapItem=> {
       let dataUser = {
        key: snapItem.key,
        value: snapItem.val()
       }
       users.push(dataUser);
        return false;
      });
      this.userList = users;
      this.loadedUserList = users;
    })
  }
  showUsers() {
    this.userRef = firebase.database().ref('users');
    this.userRef.on('value', snapshot=> {
      let users = [];
      snapshot.forEach(snapItem=> {
       let dataUser = {
        key: snapItem.key,
        value: snapItem.val()
       }
       users.push(dataUser);
        return false;
      });
      this.userList = users;
      this.loadedUserList = users;
    })
  }
  //---Search filters
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.userList = this.userList.filter((item) => {
        if (item.value.fullname) {
          return (item.value.fullname.toLowerCase().indexOf(val.toLowerCase()) > -1);
        }
      })
    }
  }
 //----End search invite users!

 //--- Upload image before submit
 uploadAllImage() {
  let storageRef = firebase.storage().ref();
    //console.log((<HTMLInputElement>document.getElementById('questionImg_'+i)).files[0]);

    for (var i = 0; i < this.counter.length; i ++) {
    if ((<HTMLInputElement>document.getElementById('questionImg_'+i)).files[0] != undefined) {
      for (let selectedFile of [(<HTMLInputElement>document.getElementById('questionImg_'+i)).files[0]]) {
        let index = i;
        let path = `/uploads/${selectedFile.name}`;
        var iRef = storageRef.child(path);
    
        iRef.put(selectedFile).then((snapshot)=> {
            this.imageGroupUpload[index] = snapshot.metadata.downloadURLs[0];
        });
      } // for upload image
    } else {
          this.imageGroupUpload[i] = "ifany";
    }
  }
    //Loading show
    let loading = this.loadingCtrl.create({
      content: 'Uploading images to firebase...'
    });

    loading.present();

    setTimeout(() => {
      loading.dismiss();
    }, 3000);

 }
  createForm(f: NgForm) {
   
    // Upload image
    console.log('Submit click');
   if (f.valid) {
    console.log('form valid');
    let dataExamForm = {};

    var dateObj = new Date();
    var month = dateObj.getUTCMonth() +1;
    var day = dateObj.getDate();
    var year = dateObj.getUTCFullYear();

    var date = month+'/'+day+'/'+year;
    var title = f.controls['title'].value;
    var totalTime = '';
    var passScore = f.controls['passScore'].value;
    var counterType = f.controls['settime'].value;
    if (f.controls['settime'].value == 'timeTotal') {
      totalTime  = f.controls['totalTime'].value;
      console.log('totaltime');
    }
    console.log(title);

    let image = [];
    let hasImage = [];
    let uploaded = [];
    let questionExamForm = [];
    let answerExamForm = [];


    for (var i = 0; i < this.counter.length; i ++) {
     console.log(i);
      answerExamForm[i] = [];
      for (var j = 0; j < this.counterAnswer[i].length; j ++) {
        answerExamForm[i].push({
          text: f.controls['answerText_'+i+'_'+j].value,
          correct: f.controls['answerValid_'+i+'_'+j].value,
          selected: false
        })
      }
     

     
      questionExamForm[i] = {
        text: f.controls['questionText_'+i].value,
        limitTime: (f.controls['answerSetTime_'+i]) ? f.controls['answerSetTime_'+i].value : "",
        type: f.controls['answerType_'+i].value,
        answer: answerExamForm[i],
        image: this.imageGroupUpload[i]
      };
     } // for counter
    
     if (this.imageGroupUpload && questionExamForm) {
      let loading = this.loadingCtrl.create({
        content: 'Creating exam ...'
      });
  
      loading.present();

      console.log(this.imageGroupUpload);
      this.af.database.ref('exams').push({
        title: title,
        totalTime: totalTime,
        date: date,
        passScore: passScore,
        userInvited: this.userInvited,
        counterType: counterType,
        question: questionExamForm,
        user: this.userid,
        scores: ""
      }).then(snapshot=> {
        setTimeout(()=> {
          loading.dismiss();
        }, 1500)
        this.navCtrl.push(TrainerListPage, {
          userid: this.userid
        });
      })
     }
      console.log(questionExamForm);
    } else {
      console.log('Error form');
    }
  }
  

  
  disabledValid(value: string) {
    return value;
  }
  
  removeQuestion(key: number) {
    this.counter.splice(key,1); 
    console.log(this.counter);
  }
  add() {
    var push = this.counter.length;
    var pushCounterAnswer = [1,2,3,4];

    this.counter.push(push);
    this.counterAnswer[push] = [1,2,3,4];

    for (var i = 0; i < this.counter.length; i ++) {
      this.imageGroupUpload[i] = 'ifany';
    }
  }
 
}