import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainerCreateExamPage } from './trainer-create-exam';

@NgModule({
  declarations: [
    TrainerCreateExamPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainerCreateExamPage),
  ],
})
export class TrainerCreateExamPageModule {}
