import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
/**
 * Generated class for the ViewResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-result',
  templateUrl: 'view-result.html',
})
export class ViewResultPage {
  examKey: string = "";
  item: any = {};
  userJoin: number = 0;
  userInvited: number = 0;
  itemUserStatus: any = [];
  fullnameUser: any = "";
  passScore: any = 0;
  userInfoData: any = [];
  constructor(public af: AngularFireDatabase ,public navCtrl: NavController, public navParams: NavParams) {

    this.af.database.ref('users').on('value', snapshot=> {
      let fullname = [];
      snapshot.forEach(snapItem=> {
        fullname[snapItem.key] = snapItem.val().fullname;
        return false;
      })
      this.fullnameUser = fullname;
    })

    if (this.navParams.get('key')) {
      let scoresField = [];
      let userExamInVited = [];
      let userExamDoneTest = [];
     

      this.examKey = this.navParams.get('key');
      this.af.database.ref('exams').orderByKey().equalTo(this.examKey).on('value', snapshot => {
        snapshot.forEach(snapItem => {
          this.item = snapItem.val();

          this.passScore = snapItem.val().passScore;
          if ((snapItem.val()).scores) {
            this.userJoin = (snapItem.val()).scores.length;
            let usersInfo = [];
            (snapItem.val()).scores.forEach(element => {
            
              userExamDoneTest.push(element.id);
              this.userInfoData[element.id] = {
                per: (element.per) ? (element.per): "#",
                pass: element.pass,
                score: (element.score) ? (element.score) : "#"
              }
            })
         
            console.log(this.userInfoData);
          }
          if ((snapItem.val()).userInvited) {
            this.userInvited = (snapItem.val()).userInvited.length;

            (snapItem.val()).userInvited.forEach(element => {
              userExamInVited.push(element)
            });
          }
          scoresField = snapItem.val().scores;

          return false;
        })

        // console.log(userExamDoneTest);
        // console.log(userExamInVited);
        for (var i = 0; i < userExamInVited.length; i ++) {
         
          let username = "";
          if (this.fullnameUser[userExamInVited[i]] != undefined) {
            username = this.fullnameUser[userExamInVited[i]];
          }
          
           if (userExamDoneTest.indexOf(userExamInVited[i]) > -1) {
            this.itemUserStatus.push({
              userid: userExamInVited[i],
              status: 'Done',
              fullname: username,
              pass: (this.userInfoData[userExamInVited[i]].pass) ? "Pass" : "Fail",
              percent: this.userInfoData[userExamInVited[i]].per,
              setColorPass: this.userInfoData[userExamInVited[i]].pass
            })
          } else {
            this.itemUserStatus.push({
              userid: userExamInVited[i],
              status: 'New',
              fullname: username,
              pass: "#",
              percent: "#"
            })
         }
         }
      //console.log(this.userInfoDone);
      console.log(this.itemUserStatus);
      });


    }


  }

  public getNameUser(userid) {
   return this.af.database.ref('users/'+userid);
  }
 

}
