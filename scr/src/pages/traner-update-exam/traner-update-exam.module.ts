import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranerUpdateExamPage } from './traner-update-exam';

@NgModule({
  declarations: [
    TranerUpdateExamPage,
  ],
  imports: [
    IonicPageModule.forChild(TranerUpdateExamPage),
  ],
})
export class TranerUpdateExamPageModule {}
