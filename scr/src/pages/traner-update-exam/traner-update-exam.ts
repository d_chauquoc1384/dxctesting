import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController  } from 'ionic-angular';

import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Reference } from '@firebase/database';
//import storage
import { Storage} from '@ionic/storage';
import { ModalController } from 'ionic-angular';


import { 
  FormGroup, 
  FormBuilder, 
  Validators, 
  NgForm
} from '@angular/forms';
import { TrainerEditModalPage } from '../trainer-edit-modal/trainer-edit-modal';
import { TrainerHeaderModalPage } from '../trainer-header-modal/trainer-header-modal';
import { TrainerQuestionPlusPage } from '../trainer-question-plus/trainer-question-plus';


/**
 * Generated class for the TranerUpdateExamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-traner-update-exam',
  templateUrl: 'traner-update-exam.html',
})
export class TranerUpdateExamPage {

  examkey         :  string  =   "";
  counter         :  any     =   [];
  counterAnswer   :  any     =   [];
  examData        :  any     =   {};
  userid          :  string  =   "";

  constructor(private alertCtrl: AlertController, public modalCtrl: ModalController, public storage: Storage ,public af: AngularFireDatabase ,public navCtrl: NavController, public navParams: NavParams) {
    this.examkey = this.navParams.get('examkey');
    
    if (this.examkey) {

      this.af.database.ref('exams').orderByKey().equalTo(this.examkey).on('value', snapshot => {
        snapshot.forEach(element => {
         this.examData = element.val();
          return false;
        });
      })
    }//EndIf ExamKey
}
removeThis(index: number) {
 if (index < 0) {
  return false;
 }
 let alert = this.alertCtrl.create({
  message: 'Are you sure to delete this question?',
  buttons: [
    {
      text: 'Cancel',
      handler: () => {
      }
    },
    {
      text: 'Ok',
      handler: () => {
        this.af.database.ref('exams/'+this.examkey+'/question/'+index).remove();
      }
    }
  ]
});
alert.present();
  
  
  // this.af.database.ref('exams/'+this.examkey+'/question/'+index).remove().then((val)=> {

  // })

}
questionPlus() {
  let modal = this.modalCtrl.create(TrainerQuestionPlusPage, {
    data: this.examData,
    examKey: this.examkey
  });
  modal.present();
}

headerEdit() {
  let modal = this.modalCtrl.create(TrainerHeaderModalPage, {
    data: this.examData,
    examKey: this.examkey
  });
  modal.present();
}

questionModal(questionData: object, num: number) {
  let modal = this.modalCtrl.create(TrainerEditModalPage, {
    questionData: questionData,
    qnumber: num,
    examKey: this.examkey
  });
  modal.present();
}



}
