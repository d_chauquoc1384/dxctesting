import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { 
  FormGroup, 
  FormBuilder, 
  Validators 
} from '@angular/forms';

import { AngularFireDatabase } from 'angularfire2/database';
import { TraineeListPage } from '../trainee-list/trainee-list';
import { Storage} from '@ionic/storage';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
/**
 * Generated class for the TraineeRegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trainee-register',
  templateUrl: 'trainee-register.html',
})
export class TraineeRegisterPage {

  public form: FormGroup;
  public items: Array<any> = [];
  public itemsRef = this.af.database.ref('users');
  public errForm: any;
  public show = false;
  public emaiExist: Array<any> = [];
  public emaiExistErr = true;
  constructor(public store : Storage, public loadingCtrl: LoadingController, public af: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      fullname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern('(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%])[A-Za-z0-9@#$%]{8,}')]],
      re_password: ['', [Validators.required, Validators.pattern('(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%])[A-Za-z0-9@#$%]{8,}')]]
    })
  }
  ngOnInit() {
    
  }
  ionViewDidLoad() {
    
  }
  isValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }
  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      loading.dismiss();
    }, 500);
  }
  traineeForm() {
    if (this.form.valid === true) {
      if (this.form.value.password === this.form.value.re_password) {
        //Load user email
        let exist = false;
        this.af.database.ref('users').on('value', snapShot=> {
            let users = [];
            snapShot.forEach(snapItem => {
              users.push(snapItem.val().email);
              return false;
            });
            if (users.indexOf(this.form.controls['email'].value) > -1) {
              exist = true;
            }
        });



      
       //Email exist checking is not existed
       if (exist == false) {
        console.log(this.emaiExist.indexOf(this.form.value.email));
        this.show = false;
        var data: any;
        data = {
          fullname: this.form.value.fullname,
          email: this.form.value.email,
          password: this.form.value.password,
          trainer: 0
        };
        let loader = this.loadingCtrl.create({
          content: "Please wait...",
          duration: 500
        });
        loader.present();
        this.itemsRef.push(data).then((val)=> {
          loader.data.content = "Register success!";
          loader.present();
          this.store.set('userid',val.key).then((val)=> {
            this.navCtrl.push(LoginPage);
          })
         
        });
        
      } else {
        let loading = this.loadingCtrl.create({
          content: 'Email existed...'
        });
        loading.present();
      
        setTimeout(() => {
          loading.dismiss();
        }, 500);
        return false;
      }

     } else { // Password confirm
       this.show = true;
       console.log(this.show);
     }
   } //form valid
    
  }
}
