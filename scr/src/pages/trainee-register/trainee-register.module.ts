import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TraineeRegisterPage } from './trainee-register';

@NgModule({
  declarations: [
    TraineeRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(TraineeRegisterPage),
  ],
})
export class TraineeRegisterPageModule {}
