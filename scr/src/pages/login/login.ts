import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, MenuController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { AngularFireDatabase } from 'angularfire2/database';
import { HomePage } from '../home/home';
import { IonicStorageModule} from '@ionic/storage';
import { Storage} from '@ionic/storage';
import { TraineeListPage } from '../trainee-list/trainee-list';
import { TrainerListPage } from '../trainer-list/trainer-list';
import { SignupPage } from '../signup/signup';




/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public form: FormGroup;
  public LoggedIn:boolean;
  private storage: Storage;
  public trainer:any;

  public itemsRef = this.af.database.ref('users');

  constructor(public loadingCtrl: LoadingController,public af: AngularFireDatabase,public navCtrl: NavController, public navParams: NavParams,public formBuilder: FormBuilder,storage:Storage,public menuCtrl: MenuController) {
    this.form=this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
    this.storage=storage;
    this.storage.get('trainer').then((val)=>{
      if(val==0){return this.trainer=0;}
      else{return this.trainer=1;}
    });
    this.storage.get('isLogin').then((isLogin)=>{
      console.log("Is LoggedIn: "+isLogin);
      this.LoggedIn=isLogin;
      if(!this.LoggedIn){
        return;
      }else{
        this.navCtrl.setRoot(HomePage);
      }
    });
    
    //this.storage.set('password','c.password');
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.menuCtrl.swipeEnable( false );
  }
  ionViewDidLeave(){
    this.menuCtrl.swipeEnable( true );
  }
  isValid(field:string){

    return !this.form.get(field).valid && this.form.get(field).touched;
  }
  signup(){
    this.navCtrl.push(SignupPage);
  }
  // logout(){
  //   this.storage.remove('username');
  //   this.storage.remove('password');
  //   this.storage.set('isLogin',false);
  //   this.navCtrl.setRoot(LoginPage);
  // }
  logIn(){
    var email=this.form.value.email;
    var pass=this.form.value.password;
    let store=this.storage;
    let nav=this.navCtrl;
    this.itemsRef.orderByChild('email').equalTo(email).on('child_added', function(data) {
      var c= data.val();
      if (c.password==pass) {
        store.set('isLogin',true);
        store.set('username',c.email);
        store.set('password',c.password);
        store.set('trainer',c.trainer);
        store.set('fullname',c.fullname);
        store.set('userid',data.key).then((val)=> {
          nav.push(HomePage);
        });
        
      }else{
        store.set('isLogin',false);
        nav.setRoot(LoginPage);
      }
      
    });
    
  }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      loading.dismiss();
    }, 500);
  }
}
