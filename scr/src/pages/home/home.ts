import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { TranslateService } from '@ngx-translate/core';
import { SignupPage } from '../signup/signup';
import { LoginPage } from '../login/login';
import { TraineeListPage } from '../trainee-list/trainee-list';
import { IonicStorageModule} from '@ionic/storage';
import { Storage} from '@ionic/storage';
import { TrainerListPage } from '../trainer-list/trainer-list';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private storage: Storage;
  language :string;
  userid: any = "";

  userData: any = {};
  constructor(public navCtrl: NavController, public af: AngularFireDatabase,public translate: TranslateService,storage:Storage) {
    this.storage=storage;
    this.language= translate.currentLang;
  
    translate.setDefaultLang('en');
    this.storage.get('userid').then((val)=>{
      this.userid = val;
     
      this.af.database.ref('users').orderByKey().equalTo(val).on('value', snapShot=> {
        snapShot.forEach(element => {
          this.userData = element.val();
          return false;
        });
      })
    });

    
    
  }
  logout(){
    this.storage.remove('username');
    this.storage.remove('password');
    this.storage.set('isLogin',false);
    this.navCtrl.setRoot(LoginPage);
  }
  changeLanguage(e){
    this.translate.use(e);
  }
  signup(){
    this.navCtrl.push(SignupPage);
  }
  login(){
    this.navCtrl.push(LoginPage);
  }
  listexam(){
    this.navCtrl.push(TraineeListPage);
  }
  listtrainer(){
    this.navCtrl.push(TrainerListPage,{
      userid: this.userid
    });
  }

  
}
