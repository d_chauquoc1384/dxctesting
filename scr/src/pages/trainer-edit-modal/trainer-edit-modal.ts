import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, AlertController  } from 'ionic-angular';
import firebase from 'firebase';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Reference } from '@firebase/database';
//import { ExamDetailPage } from '../exam-detail/exam-detail';
import { 
  FormGroup, 
  FormBuilder, 
  Validators, 
  NgForm
} from '@angular/forms';
import { TranerUpdateExamPage } from '../traner-update-exam/traner-update-exam';


/**
 * Generated class for the TrainerEditModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trainer-edit-modal',
  templateUrl: 'trainer-edit-modal.html',
})
export class TrainerEditModalPage {
  
  examKey: any = "";
  questionData: any = {};
  qNumber: number = 0;
  hasImg: boolean = false;
  image : any = "";
  examData: any = {};
  constructor(private alertCtrl: AlertController, public loadingCtrl: LoadingController ,public af: AngularFireDatabase, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams) {

    if (this.navParams.get('questionData')) {
      this.questionData = this.navParams.get('questionData');
      this.qNumber =  this.navParams.get('qnumber');
      this.examKey = this.navParams.get('examKey');

      this.af.database.ref('exams').orderByKey().equalTo(this.examKey).on('value', snapShot=> {
        snapShot.forEach(element => {
          this.examData = element.val();
          return false;
        });

        console.log(this.examData);
      })
    }
    this.image = this.questionData.image;
    //console.log(this.questionData);
  }

  editForm(form: NgForm) {
    
    if (form.valid) {
      let dataQuestionUpdate = {};
      let dataAnswer = [];

      for (var i =0; i < this.questionData.answer.length; i ++) {
        dataAnswer.push({
          correct: form.controls['answerValid_'+i].value,
          selected: 'false',
          text: form.controls['answerText_'+i].value
        })
      }
      let timeLimit = '';
      if (this.examData.counterType == 'timeTotal') {
         timeLimit = '';
      } else {
        timeLimit = form.controls['answerSetTime'].value;
      }

      dataQuestionUpdate = {
        image: this.image,
        text: form.controls['questionText'].value,
        limitTime: timeLimit,
        type: form.controls['answerType'].value,
        answer: dataAnswer
      }

      let loader = this.loadingCtrl.create({
        content: "Please wait...",
        duration: 500
      });
      loader.present();
       this.af.database.ref('exams/'+this.examKey+'/question/'+this.qNumber).update(dataQuestionUpdate).then((val) => {
        loader.data.content = "Success!";
        this.navCtrl.push(TranerUpdateExamPage, {
          examkey: this.examKey
        });
     });
    }

    //console.log('Path: exams/'+this.examKey+'/question/'+this.qNumber);
  }

  uploadImage() {
    if (this.hasImg) {
      let storageRef = firebase.storage().ref();
      let loader = this.loadingCtrl.create({
        content: "Please wait...",
        duration: 3000
      });
      loader.present();
      for (let selectedFile of [(<HTMLInputElement>document.getElementById('questionImg')).files[0]]) {
        let path = `/uploads/${selectedFile.name}`;
         var iRef = storageRef.child(path).put(selectedFile);
         if (iRef) {
            iRef.then((snapshot)=> {
              this.image = snapshot.metadata.downloadURLs[0];
              loader.data.content = "Success!";
              setTimeout(() => {
                loader.dismiss();
              }, 500);
          });
         }else {
            loader.data.content = "Upload fai, please upload again!";
            setTimeout(() => {
              loader.dismiss();
            }, 500);
         }
        
      }
      this.hasImg = false;
    }
  }
  removeImg() {
    if (this.examKey) {
      var index = this.examKey;
      var dataUpdate = {
        answer: this.questionData.answer,
        image: 'ifany',
        limitTime: this.questionData.limitTime,
        text: this.questionData.text,
        type: this.questionData.type
      }
      let alert = this.alertCtrl.create({
        message: 'Are you sure to delete this image?',
        buttons: [
          {
            text: 'Cancel',
            handler: () => {
            }
          },
          {
            text: 'Ok',
            handler: () => {
              let loader = this.loadingCtrl.create({
                content: "Please wait...",
                duration: 800
              });
              loader.present();
              this.af.database.ref('exams/'+this.examKey+'/question/'+this.qNumber).update(dataUpdate).then((val)=>{
                loader.data.content = 'Deleted!';
              })
              
              
              
              
            }
          }
        ]
      });
      alert.present();
    }
  }
  someChange() {
    if ((<HTMLInputElement>document.getElementById('questionImg')).files[0] != undefined) {
      this.hasImg = true;
    } else this.hasImg = false;
  }
  pop() {
    this.navCtrl.pop();
  }
}
