import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainerEditModalPage } from './trainer-edit-modal';

@NgModule({
  declarations: [
    TrainerEditModalPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainerEditModalPage),
  ],
})
export class TrainerEditModalPageModule {}
