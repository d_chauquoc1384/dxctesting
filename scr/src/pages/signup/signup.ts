import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TraineeRegisterPage } from '../trainee-register/trainee-register';
import { TrainerRegisterPage } from '../trainer-register/trainer-register';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  registerForTrainee() {
    this.navCtrl.push(TraineeRegisterPage);
  }
  registerForTrainer() {
    this.navCtrl.push(TrainerRegisterPage);
  }
}
