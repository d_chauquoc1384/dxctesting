import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage} from '@ionic/storage';

/**
 * Generated class for the ResultListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-result-list',
  templateUrl: 'result-list.html',
})
export class ResultListPage {
  data:any = {
    key: "",
    value: ""
  }
  public result={
    score:'',
    pass:'',
    per:''
  };
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage:Storage) {
    this.data.key=this.navParams.get('key');
    this.data.value=this.navParams.get('value');
    console.log(this.data.value);
    this.storage.get('userid').then((val)=>{
      for(var i=0; i < this.data.value.scores.length; i++){
        //console.log(!this.data.value.scores[i]);
        if(this.data.value.scores[i] && this.data.value.scores[i].id==val){
          this.result.score=this.data.value.scores[i].score;
          if(this.data.value.scores[i].pass){this.result.pass="Pass";}else{this.result.pass="Fail";}
          this.result.per=this.data.value.scores[i].per;
        }
        
      }
      console.log(this.result);
    });
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultListPage');
  }

}
