import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// Import the AF2 Module
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FirebaseConfig } from '../FirebaseConfig';
// Import the ng-translate Module
// npm install @ngx-translate/core --save
import {TranslateModule} from '@ngx-translate/core';
import { TranslateLoader } from '@ngx-translate/core';
import { TranslateService } from '@ngx-translate/core';
// npm install @ngx-translate/http-loader --save
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { SignupPage } from '../pages/signup/signup';
import { TraineeRegisterPage } from '../pages/trainee-register/trainee-register';
import { TrainerRegisterPageModule } from '../pages/trainer-register/trainer-register.module';
import { TrainerRegisterPage } from '../pages/trainer-register/trainer-register';
import { TrainerListPage } from '../pages/trainer-list/trainer-list';
import { TraineeListPage } from '../pages/trainee-list/trainee-list';
import { TrainerCreateExamPage } from '../pages/trainer-create-exam/trainer-create-exam';
import { LoginPage } from '../pages/login/login';
import { TranerUpdateExamPage } from '../pages/traner-update-exam/traner-update-exam';
//import { Camera } from '@ionic-native/camera';

//npm install ngx-pagination --save
//import {NgxPaginationModule} from 'ngx-pagination';
import { ExamPage } from '../pages/exam/exam';
//npm install --save @ionic/storage
import { IonicStorageModule } from '@ionic/storage';
import { Storage } from '@ionic/storage';
import { ViewResultPage } from '../pages/view-result/view-result';
import { TrainerEditModalPage } from '../pages/trainer-edit-modal/trainer-edit-modal';





//import { Camera } from '@ionic-native/camera';

//npm install ngx-pagination --save
//import {NgxPaginationModule} from 'ngx-pagination';

//npm install --save @ionic/storage

import { ResultListPage } from '../pages/result-list/result-list';
import { TrainerHeaderModalPage } from '../pages/trainer-header-modal/trainer-header-modal';
import { TrainerQuestionPlusPage } from '../pages/trainer-question-plus/trainer-question-plus';





// AoT requires an exported function for factories

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignupPage,
    TraineeRegisterPage,
    TrainerRegisterPage,
    TrainerListPage,
    TraineeListPage,
    TrainerCreateExamPage,
    LoginPage,
    ExamPage,
    ResultListPage,
    TranerUpdateExamPage,
    ViewResultPage,
    TrainerEditModalPage,
    TrainerHeaderModalPage,
    TrainerQuestionPlusPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FirebaseConfig),
    AngularFireDatabaseModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: createTranslateLoader,
          deps: [HttpClient]
      }
  })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignupPage,
    TraineeRegisterPage,
    TrainerRegisterPage,
    TrainerListPage,
    TraineeListPage,
    TrainerCreateExamPage,
    LoginPage,
    ExamPage,
    ResultListPage,
    TranerUpdateExamPage,
    ViewResultPage,
    TrainerEditModalPage,
    TrainerHeaderModalPage,
    TrainerQuestionPlusPage
  ],
  providers: [
    StatusBar,
    SplashScreen,,
    HttpClient,
    HttpClientModule,
    IonicStorageModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
