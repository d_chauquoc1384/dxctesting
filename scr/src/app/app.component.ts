import { Component, ViewChild } from '@angular/core';
import { Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { IonicStorageModule} from '@ionic/storage';
import { Storage} from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { AngularFireDatabase } from 'angularfire2/database';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  private storage: Storage;
  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any,icon:any}>;
  public username:string = "";
  public fullname:string = "";
  public trainer:string = "";
  language:string;

  userData: any = {};
  userid: string = "";
  userLabel: any = [];

  constructor(public af: AngularFireDatabase, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,storage:Storage,public translate: TranslateService) {
    this.initializeApp();
    this.storage=storage;
    this.language= translate.currentLang;
    this.userLabel[0] = 'Trainee';
    this.userLabel[1] = 'Trainer';
    translate.setDefaultLang('en');
    
   
      this.storage.get('userid').then((val) => {
        this.af.database.ref('users').orderByKey().equalTo(val).on('value', snapShot=> {
          snapShot.forEach(element => {
            this.userData = element.val();
            return false;
          })
        })
      })
    

    this.pages = [
      { title: 'home', component: HomePage,icon:'home' },
    ];
    this.storage.get('username').then((val)=>{return this.username= val});
    this.storage.get('fullname').then((val)=>{

      return this.fullname= val;
    });
    this.storage.get('trainer').then((val)=>{
      if(val == 0) {
        return this.trainer='Trainee';
      } else {
        return this.trainer='Trainer';
      }
    });
  }
  changeLanguage(e){
    this.translate.use(e);
  }
  logout(){
    this.storage.remove('username').then((val) => {
      this.username = "";
    });
    this.storage.remove('password');
    this.storage.remove('fullname').then((val)=> {
      this.fullname = "";
    });
    this.storage.remove('trainer').then((val)=> {
      this.trainer = "";
    });
    this.storage.set('isLogin',false);
    this.nav.setRoot(LoginPage);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
